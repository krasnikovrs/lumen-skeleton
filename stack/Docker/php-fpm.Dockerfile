ARG PHP_VERSION=7.4

FROM itdevgroup/php-fpm${PHP_VERSION}


###########################################################################
# xDebug:
###########################################################################

ARG INSTALL_XDEBUG=false

RUN if [ ${INSTALL_XDEBUG} = true ]; then \
      apt-get update -yqq \
      && apt-get install -yqq \
      php${PHP_VERSION}-xdebug \
;fi
