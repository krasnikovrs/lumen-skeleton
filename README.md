#Backend skeleton

Backend skeleton on Lumen framework

## Install
- Clone repository `git clone git@github.com:krasnikov87/lumen-skeleton.git`
- Run docker-compose `docker-compose up -d`
- Install vendors `docker-compose exec php-fpm php artisan composer install`
- Create `.env` `cp .env.example .env`


## TODO
- Admin auth
    - Login
    - Logout
    - Forgot password
    - Reset password
    - Activate account
    - Auth token revoke
- User CRUD
- User permissions
- Settings module
- Mail sender module
- Socket module
- Logs module
    - Systems logs
    - Horizon details
