<?php

declare(strict_types=1);

if (! function_exists('app_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function app_path($path = '')
    {
        return rtrim(app()->basePath('app/'.$path), DIRECTORY_SEPARATOR);
    }
}

if (! function_exists('config_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function config_path($path = '')
    {
        return rtrim(app()->basePath('config/'.$path), DIRECTORY_SEPARATOR);
    }
}
if (! function_exists('public_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function public_path($path = '')
    {
        return rtrim(app()->basePath('public/'.$path), DIRECTORY_SEPARATOR);
    }
}
